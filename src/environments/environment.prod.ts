export const environment = {
  production: true,
  iconsPath: '/assets/svg',
  engineTag: 'entropy_engine',
  gitlab: {
    oauth: {
      uri: 'https://gitlab.com/oauth/authorize',
      query: {
        client_id: 'd8439bbf8865cd0382f9475acc0c8093227e81e91ddaef9aa4a9715ada6ac978',
        response_type: 'token',
      }
    },
    api: {
      method: 'POST',
      uri: 'https://gitlab.com/api/graphql',
    }
  }
};
