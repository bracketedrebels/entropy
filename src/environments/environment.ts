// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  iconsPath: '/assets/svg',
  engineTag: 'entropy_engine',
  gitlab: {
    oauth: {
      uri: 'https://gitlab.com/oauth/authorize',
      query: {
        client_id: 'd8439bbf8865cd0382f9475acc0c8093227e81e91ddaef9aa4a9715ada6ac978',
        response_type: 'token',
      }
    },
    api: {
      gql: {
        method: 'POST',
        uri: 'https://gitlab.com/api/graphql',
      },
      rest: {
        uri: 'https://gitlab.com/api/v4',
      }
    }
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
