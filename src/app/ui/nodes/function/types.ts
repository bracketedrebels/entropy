export type FunctionDescriptor<T = any> = {
  name: string;
  description?: string;
  input: FunctionPropertyDescriptor[];
  return: {
    description: string;
    type: T;
  };
}

export type FunctionPropertyDescriptor<T = any> = {
  name: string;
  description?: string;
  type: T;
}
