import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Component } from './component';
import { IconsModule } from 'src/kit/icon';

@NgModule({
  imports: [ CommonModule, IconsModule ],
  declarations: [ Component ],
  exports: [ Component ]
})
export class Module { }
