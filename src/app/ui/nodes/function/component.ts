import { Component as NgComponent, Input, ChangeDetectionStrategy, ViewEncapsulation, Output, EventEmitter, ContentChildren, QueryList } from '@angular/core';
import { FunctionDescriptor, FunctionPropertyDescriptor } from './types';

@NgComponent({
  selector: 'brs-node-function',
  exportAs: 'brs-node-function',
  templateUrl: 'component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  host: {
    class: `
      rounded bg-gradient-br-surface
      flex flex-col justify-start items-stretch`
  }
})
export class Component {
  @Input() public descriptor: FunctionDescriptor;
  @Input() public collapsed: boolean = false;
  @Input() public detailed: boolean = false;

  public readonly slotTracker = (_, v: FunctionPropertyDescriptor) => v.name;
}