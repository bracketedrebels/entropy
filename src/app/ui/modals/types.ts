import { TemplateRef } from '@angular/core';
import { Subject } from 'rxjs';

export type ModalEntry<T = any> = [TemplateRef<T>, T?];
export type ModalTemplatePayload<In, Out> = {
  in?: Subject<In>,
  out: Subject<Out>,
}
