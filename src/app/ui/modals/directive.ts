import { Directive as NgDirective, Input, OnChanges, SimpleChanges, TemplateRef } from '@angular/core'
import { Service } from './service'

@NgDirective({
  selector: '[brs-modal-dialog]',
  exportAs: 'brs-modal-dialog',
})
export class Directive implements OnChanges {
  @Input('brs-modal-dialog') public active: boolean

  public ngOnChanges({active}: SimpleChanges): void {
    if (active) this.modals.activate(this.template)
    else this.modals.deactivate(this.template)
  }

  constructor(
    private readonly template: TemplateRef<any>,
    private readonly modals: Service
  ) {}
}