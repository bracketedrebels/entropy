import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Component } from './component';

@NgModule({
  imports: [ CommonModule ],
  declarations: [ Component ],
  exports: [ Component ]
})
export class Module { }
