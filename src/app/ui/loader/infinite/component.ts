import { Component as NgComponent, ChangeDetectionStrategy, Input } from '@angular/core';
import { fadeInOnEnterAnimation, fadeOutOnLeaveAnimation } from 'angular-animations';

@NgComponent({
  selector: '[brs-loader-infinite]',
  templateUrl: 'component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  host: { class: 'relative z-default' },
  animations: [
    fadeInOnEnterAnimation(),
    fadeOutOnLeaveAnimation()
  ]
})
export class Component {
  @Input() public active = false;
}
