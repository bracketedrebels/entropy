import { Directive as NgDirective, Input, HostBinding } from '@angular/core';

@NgDirective({
  selector: '[brs-button]',
  exportAs: 'brs-button'
})
export class Directive {
  @Input() public selected = false;
  @Input() public disabled = false;
}
