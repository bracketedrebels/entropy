import { Component as NgComponent, ChangeDetectionStrategy, ViewEncapsulation, Input, ContentChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { HelixPoint } from 'src/kit/helix';
import { Directive as AnchorDirective } from './anchor.directive';
import { Directive as PennantDirective } from './pennant.directive';

@NgComponent({
  selector: '[brs-contextual]',
  exportAs: 'brs-contextual',
  templateUrl: 'component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None
})
export class Component {
  @Input() public joint: [HelixPoint, HelixPoint] = [9, 9];
  @Input() public active = false;

  @Output() public readonly active$ = new EventEmitter<boolean>();
  
  @ContentChild(AnchorDirective, { static: false }) public anchor: AnchorDirective;
  @ContentChild(PennantDirective, { static: false }) public pennant: PennantDirective;

  public closer(): () => void { return () => this.active$.emit(false) }
  public opener(): () => void { return () => this.active$.emit(true) }
  public toggler(): (v: boolean) => void { return v => this.active$.emit(v) }

  constructor( public readonly eref: ElementRef<HTMLElement> ) {}
}