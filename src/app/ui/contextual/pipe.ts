import { Pipe as NgPipe, PipeTransform, ElementRef } from '@angular/core';
import { Observable, asyncScheduler } from 'rxjs';
import { ViewportService } from 'src/kit/viewport';
import { HelixPoint, helixInBox } from 'src/kit/helix';
import { fromClientRect, mul, sub } from 'src/kit/vector';
import { observeOn, map } from 'rxjs/operators';
import { helixNormalized } from 'src/kit/helix/consts';

@NgPipe({
  name: 'place',
})
export class Pipe implements PipeTransform {
  public transform(joint: [HelixPoint, HelixPoint], anchor: ElementRef<HTMLElement>): Observable<{ top: string; left: string; transform: string }> {
    return this.vpservice.resized$
      .pipe(
        observeOn(asyncScheduler),
        map(() => {
          const ancorRect = anchor.nativeElement.getBoundingClientRect();
          const [left, top] = helixInBox({
            box: fromClientRect(ancorRect),
            helix: joint[0] })
          const [x, y] = mul(mul(helixNormalized(joint[1]), [-1, -1]), [100, 100])
          
          return {
            top: `${top}px`,
            left: `${left}px`,
            transform: `translate(${x}%, ${y}%)`,
            width: `${ancorRect.width}px`
          }
        }))
  }

  constructor( private readonly vpservice: ViewportService ) { }
}