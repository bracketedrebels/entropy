import { Directive as NgDirective, TemplateRef } from '@angular/core'

@NgDirective({
  selector: '[brs-anchor]',
  exportAs: 'brs-anchor',
})
export class Directive {
  constructor( public readonly template: TemplateRef<any> ) {}
}