import { Injectable } from '@angular/core';
import { DelegationService } from 'src/kit/delegate';

@Injectable({ providedIn: 'root' })
export class Service extends DelegationService { }