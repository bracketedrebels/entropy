import { Directive as NgDirective, Input, OnChanges, SimpleChanges, TemplateRef, ElementRef } from '@angular/core'
import { Service } from './service'

@NgDirective({
  selector: '[active]',
  exportAs: 'active',
})
export class Directive implements OnChanges {
  @Input() public active = false

  public ngOnChanges({active}: SimpleChanges): void {
    if (active.currentValue) this.service.activate(this.template)
    else this.service.deactivate(this.template)
  }

  constructor(
    private readonly template: TemplateRef<any>,
    private readonly service: Service
  ) {}
}