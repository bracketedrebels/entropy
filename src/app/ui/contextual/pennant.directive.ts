import { Directive as NgDirective, TemplateRef } from '@angular/core'

@NgDirective({
  selector: '[brs-pennant]',
  exportAs: 'brs-pennant',
})
export class Directive {
  constructor( public readonly template: TemplateRef<any> ) {}
}