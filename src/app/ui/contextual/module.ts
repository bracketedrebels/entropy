import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Directive } from './directive';
import { Component } from './component';
import { Pipe } from './pipe';

@NgModule({
  imports: [ CommonModule ],
  declarations: [ Directive, Component, Pipe ],
  exports: [ Component ]
})
export class Module { }