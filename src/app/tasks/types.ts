export type TaskDescriptor<Params = any, Result = any> = {
  execute(v: Params): Promise<Result>;
}