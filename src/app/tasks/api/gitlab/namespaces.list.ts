// import { switchMap } from 'rxjs/operators';
// import { fullGitlabRESTUri, BorderErrors } from '../consts';
// import { throwError, of } from 'rxjs';
// import { TaskDescriptor } from '../../types';
// import { HttpClient } from '@angular/common/http';
// import { AuthorizationService } from 'src/app/authorization';

// export default class implements TaskDescriptor<never, ResponseItem> {
//   public execute(): Promise<ResponseItem> {
//     return this.auth.getToken
//   }

//   constructor(
//     private readonly http: HttpClient,
//     private readonly auth: AuthorizationService,
//   ) { }
// }

// // export default () => (
// //   ({gitlab: { http, token$ }}) => token$
// //     .pipe(
// //       switchMap(v => http
// //         .get(
// //           fullGitlabRESTUri('/namespaces'),
// //           { headers:
// //             { 'Authorization': `Bearer ${v}`
// //             }
// //           }
// //         )
// //       )
// //     , switchMap(items => Array.isArray(items) && items.every(v => validate(v))
// //       ? of(items)
// //       : throwError(BorderErrors.ResponseValidationFailure))
// //     )
// //   ) as APICommand<ResponseItem[]>

// export type ResponseItem = {
//   id: number;
//   name: string;
//   path: string;
//   kind: ('user' | 'group') & string;
//   full_path: string;
// }

// function validate(data: any): data is ResponseItem {
//   return true
//     && typeof data.id === 'number'
//     && typeof data.name === 'string'
//     && typeof data.path === 'string'
//     && typeof data.full_path === 'string'
//     && (data.kind === 'user' || data.kind === 'group')
// }
