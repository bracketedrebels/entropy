import { APICommand } from '../types';
import { switchMap, map, tap } from 'rxjs/operators';
import { fullGitlabRESTUri, BorderErrors } from '../consts';
import { throwError, of } from 'rxjs';

export default () => (
  ({gitlab: { http, token$ }}) => token$
    .pipe(
      switchMap(v => http
        .get(
          fullGitlabRESTUri('/projects'),
          { params:
            { archived: 'false'
            , order_by: 'last_activity_at'
            , simple: 'true'
            , membership: 'true'
            }
          , headers:
            { 'Authorization': `Bearer ${v}`
            }
          }
        )
      )
    , switchMap(items => Array.isArray(items) && items.every(v => validate(v))
      ? of(items)
      : throwError(BorderErrors.ResponseValidationFailure))
    )
  ) as APICommand<ResponseItem[]>

type ResponseItem = {
  id: number;
  description: string;
  ssh_url_to_repo: string;
  http_url_to_repo: string;
  web_url: string;
  tag_list: string[];
  name: string;
  name_with_namespace: string;
  created_at: string;
  last_activity_at: string;
  avatar_url: string | null;
}

function validate(data: any): data is ResponseItem {
  return true
    && typeof data.id === 'number'
    && typeof data.description === 'string'
    && typeof data.ssh_url_to_repo === 'string'
    && typeof data.http_url_to_repo === 'string'
    && typeof data.web_url === 'string'
    && Array.isArray(data.tag_list) && data.tag_list.every(v => typeof v === 'string')
    && typeof data.name === 'string'
    && typeof data.name_with_namespace === 'string'
    && typeof data.created_at === 'string'
    && typeof data.last_activity_at === 'string'
    && (data.avatar_url === null || typeof data.avatar_url === 'string')
}
