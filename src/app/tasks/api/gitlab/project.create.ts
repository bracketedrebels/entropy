import { APICommand } from '../types';
import { switchMap } from 'rxjs/operators';
import { fullGitlabRESTUri } from '../consts';

export default ({name}: { name: string }) => (
  ({gitlab: { http, token$ }}) => token$
    .pipe(
      switchMap(v => http
        .post(
          fullGitlabRESTUri('/projects'),
          '',
          { params:
            { name
            }
          , headers:
            { 'Authorization': `Bearer ${v}`
            }
          }
        )
      )
    )
  ) as APICommand<any>
