import { Directive as NgDirective, TemplateRef } from '@angular/core';

@NgDirective({
  selector: '[caption]',
  exportAs: 'caption'
})
export class Directive {
  constructor( public readonly template: TemplateRef<void> ) { }
}
