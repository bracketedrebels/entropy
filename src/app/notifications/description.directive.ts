import { Directive as NgDirective, TemplateRef } from '@angular/core';

@NgDirective({
  selector: '[description]',
  exportAs: 'description'
})
export class Directive {
  constructor( public readonly template: TemplateRef<void> ) { }
}
