import { NgModule } from '@angular/core';
import { Component } from './component';
import { Directive as ActionDirective } from './action.directive';
import { Directive as CaptionDirective } from './caption.directive';
import { Directive as DescriptionDirective } from './description.directive';
import { Directive as NotificationDirective } from './notification.directive';
import { CommonModule } from '@angular/common';
import { Pipe as PortionPipe } from './portion.pipe';

@NgModule({
  imports: [ CommonModule ],
  declarations: [ Component, ActionDirective, CaptionDirective, DescriptionDirective, PortionPipe, NotificationDirective ],
  exports: [ Component, ActionDirective, CaptionDirective, DescriptionDirective, PortionPipe, NotificationDirective ]
})
export class Module { }
