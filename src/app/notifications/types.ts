import { TemplateRef } from '@angular/core';

export type NotificationDescriptor = {
  caption: TemplateRef<void>;
  description?: TemplateRef<void>;
  duration?: number;
  actions?: Array<{
    action: () => any,
    caption: TemplateRef<void>
  }>;
};
