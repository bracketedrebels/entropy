import { Injectable, TemplateRef } from '@angular/core';
import { Subject, Observable, asyncScheduler } from 'rxjs';
import { scan, share, combineLatest, map, shareReplay, startWith, observeOn, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class Service {
  public enqueue(value: TemplateRef<void>): void { this.subjectEnqueue.next(value) }
  public complete(value: TemplateRef<void>, action: () => any): void { this.subjectComplete.next([value, action]) }
  public readonly notifications$: Observable<TemplateRef<void>[]>;

  constructor( ) {
    this.notifications$ = this.subjectEnqueue
      .pipe(
        observeOn(asyncScheduler),
        scan((acc, v: TemplateRef<void>) => [...acc, v], [] as TemplateRef<void>[]),
        share(),
        combineLatest(this.subjectComplete
          .pipe(
            observeOn(asyncScheduler),
            startWith([]))),
        map(([nots, [omit]]) => nots
          .filter(v => v !== omit)),
        startWith([]),
        shareReplay(1));
  }

  private readonly subjectEnqueue = new Subject<TemplateRef<void>>();
  private readonly subjectComplete = new Subject<[TemplateRef<void>, () => any]>();
}
