import { Directive as NgDirective, TemplateRef, Input } from '@angular/core';

@NgDirective({
  selector: '[action]',
  exportAs: 'action'
})
export class Directive {
  @Input() public action: () => void;
  constructor( public readonly template: TemplateRef<void> ) { }
}
