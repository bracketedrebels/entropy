import { Pipe as NgPipe, PipeTransform, TemplateRef } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@NgPipe({
  name: 'portion',
  pure: true
})
export class Pipe implements PipeTransform {
  public transform(source: Observable<TemplateRef<void>[]>, windowsSize?: number): typeof source {
    return source
      .pipe(
        map(v => v.slice(0, windowsSize || 1)))
  }
}
