import { Input, Directive as NgDirective, TemplateRef } from '@angular/core';

@NgDirective({
  selector: '[notification]',
  exportAs: 'notification'
})
export class Directive {
  constructor( public readonly template: TemplateRef<void> ) { }
}
