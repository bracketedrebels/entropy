import { Component as NgComponent, ChangeDetectionStrategy, Input, ContentChild, ContentChildren, QueryList, Output, EventEmitter, ElementRef, OnInit } from '@angular/core';
import { Directive as ActionDirective } from './action.directive';
import { Directive as CaptionDirective } from './caption.directive';
import { Directive as DescriptionDirective } from './description.directive';
import { Subscription, fromEvent, merge, interval, NEVER } from 'rxjs';
import { startWith, map, switchMap, take } from 'rxjs/operators';
import { trigger, transition, style, animate } from '@angular/animations';

@NgComponent({
  selector: '[brs-notification]',
  templateUrl: 'component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class Component implements OnInit {
  @Input() public duration = 5000; // ms
  @ContentChild(CaptionDirective, { static: true }) public caption: CaptionDirective;
  @ContentChild(DescriptionDirective, { static: true }) public description: DescriptionDirective;
  @ContentChildren(ActionDirective) public actions: QueryList<ActionDirective>;

  @Output() public readonly dismiss$ = new EventEmitter<void>();

  public ngOnInit(): void {
    this.subscription =
      merge(
        fromEvent(this.eref.nativeElement, 'pointerleave'),
        fromEvent(this.eref.nativeElement, 'pointerenter'))
      .pipe(
        map(v => v.type === 'pointerleave'),
        startWith(true),
        switchMap(v => v
          ? interval(this.duration)
          : NEVER),
        take(1))
      .subscribe(() => this.dismiss$.emit());
  }

  constructor( private readonly eref: ElementRef<HTMLDivElement> ) { }

  

  private subscription: Subscription;
}
