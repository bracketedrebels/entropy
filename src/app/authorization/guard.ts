import { ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanActivate } from '@angular/router';
import { Injectable } from '@angular/core';
import { LocalStorageService } from '../storage';
import { Observable, of } from 'rxjs';
import { map, tap, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class Guard implements CanActivate {
  canActivate(next: ActivatedRouteSnapshot, snapshot: RouterStateSnapshot): Observable<boolean> {
    const { access_token, state, token_type } = (next.fragment || '')
      .split('&')
      .map(v => v.split('='))
      .reduce((acc, [k, v]) => ({
        ...acc,
        [k]: v
      }), {} as {[i: string]: string});
    if (access_token && state) {
      if (token_type !== 'bearer') {
        // @TODO: introduce error codes and links to the online documentation by the error code.
        // @TODO: autocollect debug information
        throw new Error(`Unsupported token type '${token_type}'. Please, contact the support`)
      }
      return this.localStoageService
        .get('oauthState')
        .pipe(
          map(v => v === state),
          tap(v => v && this.localStoageService.set('accessToken', access_token)),
          tap(() => this.router.navigate(['.'], { preserveFragment: false })),
          map(() => false),
          take(1))
    }
    return of(true).pipe( take(1) );
  }

  constructor(
    private readonly localStoageService: LocalStorageService,
    private readonly router: Router,
  ) { }
}