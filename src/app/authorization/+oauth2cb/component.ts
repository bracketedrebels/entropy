import { Component as NgComponent, ChangeDetectionStrategy, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { take, switchMap, map, share, tap } from 'rxjs/operators';
import { of, combineLatest, Subject, interval, BehaviorSubject } from 'rxjs';
import { LocalStorageService } from 'src/app/storage';
import { Key } from '../consts';
import { SupportedProvider } from '../types';

@NgComponent({
  templateUrl: 'component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class Component {
  public readonly state$ = new BehaviorSubject<{ code?: 0 | 1; provider?: SupportedProvider; }>({})
  public readonly timeout$ = this.state$
    .pipe(
      take(1),
      switchMap(() => interval(1000)),
      take(10),
      map(v => 10 - v),
      share())

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly localStorage: LocalStorageService,
  ) {
    combineLatest(
      this.localStorage.get(Key.state),
      this.localStorage.get(Key.provider),
      this.activatedRoute.fragment
        .pipe(
          map(v => new URLSearchParams(v)),
          map(v => ({
            token: v.get('access_token'),
            state: v.get('state') }))))
    .pipe(
      switchMap(([pendingState, provider, {token, state}]) => state === pendingState
        ? of({
            token: token,
            code: 0 as 0,
            provider: provider })
        : of({
            code: 1 as 1,
            provider: provider })),
      take(1))
    .subscribe(v => {
      this.localStorage.remove([Key.state, Key.provider])
      if (v.code === 0) this.localStorage.set(v.provider, v.token)
      this.state$.next(v)
    })
      
    this.timeout$.subscribe({ complete: () => window.close() })
  }
}
