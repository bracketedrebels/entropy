import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { Component } from './component';
import { IconsModule } from 'src/kit/icon';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    RouterModule.forChild([{
      path: '',
      component: Component,
    }]),
    IconsModule,
    CommonModule
  ],
  declarations: [ Component ],
})
export class Module { }
