import { Key } from './consts';

/**
 * https://tools.ietf.org/html/rfc6749#section-4.1
 */
export type WebApplicationFlowResponse = {
  access_token: string;
  token_type: 'bearer';
  expires_in: number;
  refresh_token: string;
}

export type SupportedProvider = Key.tokenGitlab
