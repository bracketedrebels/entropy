export const randomToken = (size: number) => (new Array(size))
  .fill('')
  .map(() => Math
    .random()
    .toString(16)
    .split('')
    .pop())
  .join('')

export const enum Key {
  provider = 'oauth2-pending-provider',
  tokenGitlab = 'oauth2-0',
  state = 'oauth2-pending-state',
}
