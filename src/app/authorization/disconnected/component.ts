import { Input, Component as NgComponent, ChangeDetectionStrategy, ViewEncapsulation } from '@angular/core';
import { Service } from '../service';
import { SupportedProvider } from '../types';
import { map, tap } from 'rxjs/operators';
import { Key } from '../consts';
import { fadeInOnEnterAnimation, fadeOutOnLeaveAnimation } from 'angular-animations';

@NgComponent({
  selector: '[brs-provider-connection-required]',
  templateUrl: 'component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  host: { class: 'relative z-default' },
  animations: [
    fadeOutOnLeaveAnimation(),
    fadeInOnEnterAnimation()
  ]
})
export class Component {
  @Input() public provider: SupportedProvider | undefined = undefined

  public readonly required$ = this.service.tokens$
    .pipe(
      map(tokens => this.provider
        ? tokens.findIndex(([provider]) => provider === this.provider) < 0
        : tokens.length === 0))

  public connectToGitlab(): void { this.service.riseAuthorizationDialog(Key.tokenGitlab) }

  constructor(
    public readonly service: Service
  ) { }
}
