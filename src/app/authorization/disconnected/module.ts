import { NgModule } from '@angular/core';
import { Component } from './component';
import { CommonModule } from '@angular/common';
import { IconsModule } from 'src/kit/icon';

@NgModule({
  imports: [ CommonModule, IconsModule ],
  declarations: [ Component ],
  exports: [ Component ]
})
export class Module { }