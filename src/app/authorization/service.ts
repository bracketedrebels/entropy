import { Injectable } from '@angular/core';
import { LocalStorageService } from '../storage';
import { switchMap, take, pluck, filter, tap, distinctUntilChanged, map, startWith } from 'rxjs/operators'
import { Observable } from 'rxjs';
import { randomToken, Key } from './consts';
import { environment } from 'src/environments/environment';
import { SupportedProvider } from './types';

@Injectable({
  providedIn: 'root'
})
export class Service {
  public readonly tokens$ = this.localStorage.storage$
    .pipe(
      distinctUntilChanged((a, b) => a[Key.tokenGitlab] === b[Key.tokenGitlab]),
      map(v => (Key.tokenGitlab in v
        ? [[Key.tokenGitlab, v[Key.tokenGitlab]]]
        : []) as [SupportedProvider, string][]),
      startWith([] as [SupportedProvider, string][]))

  public getToken(provider: SupportedProvider): Observable<string> {
    return this.localStorage.storage$
      .pipe(
        pluck(provider),
        filter(v => !!v),
        take(1))
  }

  public riseAuthorizationDialog(provider: SupportedProvider): void {
    this.localStorage
      .set(Key.provider, provider)
      .pipe(
        switchMap(() => this.localStorage
          .set(Key.state, randomToken(64))))
      .subscribe(v => window
        .open(`${
          environment.gitlab.oauth.uri}?client_id=${
          environment.gitlab.oauth.query.client_id}&response_type=${
          environment.gitlab.oauth.query.response_type}&redirect_uri=${
          location.origin}/oauth2cb&state=${v}`))
  }

  constructor(
    private readonly localStorage: LocalStorageService,
  ) { }
}