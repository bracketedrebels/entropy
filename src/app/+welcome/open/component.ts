import { Component as NgComponent, ChangeDetectionStrategy, ViewEncapsulation, Input, Output, EventEmitter } from '@angular/core';
import { fadeOutCollapseOnLeaveAnimation, fadeInExpandOnEnterAnimation } from 'angular-animations';
import { AuthorizationService } from 'src/app/authorization';
import { map } from 'rxjs/operators';

@NgComponent({
  selector: '[brs-project-open]',
  templateUrl: 'component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  host: { class: "contents" },
  animations: [
    fadeOutCollapseOnLeaveAnimation(),
    fadeInExpandOnEnterAnimation()
  ]
})
export class Component {
  @Input() public expanded = false;
  @Input() public discovered = [];

  constructor(
    private readonly authorization: AuthorizationService
  ) {}
}
