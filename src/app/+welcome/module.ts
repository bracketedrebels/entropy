import { NgModule } from '@angular/core';
import { ButtonModule } from '../ui/button';
import { CommonModule } from '@angular/common';
import { Component } from './component';
import { RouterModule } from '@angular/router';
import { IconsModule } from 'src/kit/icon';
import { VariableModule } from 'src/kit/variable';
import { ProjectCreateModule } from './create';
import { InfiniteLoaderModule } from '../ui/loader/infinite';
import { ProjectOpenModule } from './open';

@NgModule({
  imports: [
    ButtonModule,
    CommonModule,
    IconsModule,
    VariableModule,
    ProjectCreateModule,
    ProjectOpenModule,
    InfiniteLoaderModule,
    RouterModule.forChild([{
      path: '',
      component: Component
    }]) ],
  declarations: [ Component ],
  exports: [ Component ]
})
export class Module { }
