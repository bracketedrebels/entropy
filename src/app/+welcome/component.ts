import { Component as NgComponent, ChangeDetectionStrategy, ViewEncapsulation } from '@angular/core';
import { ProjectService } from '../project';
import { APIService } from '../api';
import projectsList from '../api/gitlab/projects.list';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import namespacesList from '../api/gitlab/namespaces.list';

@NgComponent({
  templateUrl: 'component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  host: { class: 'contents' },
})
export class Component {
  public createProject(name: string): void { this.project.create({name}) }
  public readonly projectsList$ = this.api
    .executeCommand(projectsList())
    .pipe(
      map(projects => projects
        .filter(v => v.tag_list
          .includes(environment.engineTag))));
  
  public readonly namespaces$ = this.api
    .executeCommand(namespacesList())

  constructor(
    private readonly project: ProjectService,
    private readonly api: APIService
  ) {}
}