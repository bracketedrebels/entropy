import { NgModule } from '@angular/core';
import { VariableModule } from 'src/kit/variable';
import { CommonModule } from '@angular/common';
import { IconsModule } from 'src/kit/icon';
import { Component } from './component';
import { ProviderConnectionRequiredModule } from 'src/app/authorization/disconnected';
import { AssistModule } from 'src/app/ui/contextual';

@NgModule({
  imports: [
    VariableModule,
    CommonModule,
    IconsModule,
    ProviderConnectionRequiredModule,
    AssistModule
  ],
  declarations: [ Component ],
  exports: [ Component ]
})
export class Module { }
