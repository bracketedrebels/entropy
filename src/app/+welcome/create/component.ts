import { Component as NgComponent, ChangeDetectionStrategy, ViewEncapsulation, Input, Output, EventEmitter } from '@angular/core';
import { fadeOutCollapseOnLeaveAnimation, fadeInExpandOnEnterAnimation } from 'angular-animations';
import { ResponseItem } from 'src/app/api/gitlab/namespaces.list';

@NgComponent({
  selector: '[brs-project-create]',
  templateUrl: 'component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  host: { class: "contents" },
  animations: [
    fadeOutCollapseOnLeaveAnimation(),
    fadeInExpandOnEnterAnimation()
  ]
})
export class Component {
  @Input() public namespaces: ResponseItem[] = []
  @Input() public expanded = false
  @Output() public readonly requested = new EventEmitter<string>()

  public nsname = ''
  public pname = ''
  public nsassisting = false

  public updateNS(v: string): void { this.nsname = v }
}
