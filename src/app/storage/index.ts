export { StorageService as LocalStorageService } from './local.storage';
export { StorageService as SessionStorageService } from './session.storage';
