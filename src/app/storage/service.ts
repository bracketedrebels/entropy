import { OnDestroy } from '@angular/core';
import { Observable, BehaviorSubject, Subject, fromEvent } from 'rxjs';
import { withLatestFrom, map, distinctUntilChanged, take, tap, filter } from 'rxjs/operators';

export class Service<T extends {[i: string]: any}> implements OnDestroy {
  public readonly storage$: Observable<T>;
  public set(key: keyof T, value: T[typeof key]): Observable<T[typeof key]> {
    this.subjectSet.next({key, value});
    return this.get(key);
  }

  public get(key: keyof T): Observable<T[typeof key]>;
  public get(): Observable<T>;
  public get(key?: keyof T): Observable<T[typeof key]> | Observable<T> {
    return key
      ? this.subjectStorage
          .pipe(
            map(v => v[key]),
            take(1))
      : this.subjectStorage;
  }

  public exists(key: keyof T): Observable<boolean> {
    return this.subjectStorage
      .pipe(
        map(v => key in v),
        take(1))
  }

  public remove(keys: (keyof T)[]): Observable<T> {
    this.subjectRemove.next(keys);
    return this.storage$;
  }

  public clear(): Observable<T> {
    this.storageInstance.clear();
    this.subjectStorage.next({} as T);
    return this.storage$;
  }

  public ngOnDestroy(): void {
    this.subscriptions.forEach(v => v.unsubscribe());
    this.subscriptions.length = 0;
  }

  constructor(private readonly storageInstance: Storage ) {
    this.storage$ = this.subjectStorage;
  }

  private readonly subjectSet = new Subject<{key: keyof T; value: T[keyof T]}>();
  private readonly subjectRemove = new Subject<(keyof T)[]>();
  private readonly subjectStorage = new BehaviorSubject<T>((new Array(this.storageInstance.length))
    .fill(null)
    .reduce((acc, _, i) => ({
      ...acc,
      [this.storageInstance.key(i)]: this.storageInstance.getItem(this.storageInstance.key(i))
    }), {}));

  private readonly subscriptions = [
    this.subjectSet
      .pipe(
        withLatestFrom(this.subjectStorage))
      .subscribe(([{ key, value }, cache]) => {
        this.storageInstance.setItem(key as string, value);
        this.subjectStorage.next({
          ...cache,
          [key]: value
        });
      }),
    this.subjectRemove
      .pipe(
        withLatestFrom(this.subjectStorage))
      .subscribe(([keys, cache]) => {
        keys
          .forEach(key => this.storageInstance
            .removeItem(key as string))
        this.subjectStorage
          .next(Object
            .keys(cache)
            .reduce((acc, key) => keys
              .includes(key)
                ? acc
                : {...acc, [key]: cache[key]}, {} as typeof cache));
      }),
    fromEvent(window, 'storage')
      .pipe(
        filter((e: StorageEvent) => e.storageArea === this.storageInstance),
        withLatestFrom(this.subjectStorage))
      .subscribe(([e, cache]) => this.subjectStorage
        .next({ ...cache, [e.key]: e.newValue }))
  ]
}
