import { Service } from './service'
import { Injectable } from '@angular/core'
import { LocalStorageDescriptor } from './types';

@Injectable({
  providedIn: "root"
})
export class StorageService extends Service<LocalStorageDescriptor> {
  constructor() { super(localStorage) }
}