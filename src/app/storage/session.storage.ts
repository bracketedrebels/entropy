import { Service } from './service'
import { Injectable } from '@angular/core'
import { SessionStorageDescriptor } from './types'

@Injectable({
  providedIn: "root"
})
export class StorageService extends Service<SessionStorageDescriptor> {
  constructor() { super(sessionStorage) }
}