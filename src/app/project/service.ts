import { Injectable } from '@angular/core'
import { BehaviorSubject, Observable } from 'rxjs'
import { ProjectMetaDescriptor } from './types'
import { APIService } from '../api';
import projectCreate from '../api/gitlab/project.create';
import { take, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class Service {
  public create({name}: {name: string}) {
    this.api
      .executeCommand(projectCreate({name}))
      .pipe(
        take(1))
      .subscribe(v => this.meta$.next(v));
  }
  public open(): Observable<ProjectMetaDescriptor> { return }

  public readonly meta$ = new BehaviorSubject<ProjectMetaDescriptor | null>(null)

  constructor(
    private readonly api: APIService
  ) { }
}
