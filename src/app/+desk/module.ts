import { NgModule } from '@angular/core';

import { Component } from './component';
import { IconsModule } from 'src/kit/icon';
import { VariableModule } from 'src/kit/variable';
import { FunctionNodeModule } from '../ui/nodes/function';
import { AssetsModule } from '../assets';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ButtonModule } from '../ui/button';

@NgModule({
  imports: [
    IconsModule,
    ButtonModule,
    VariableModule,
    FunctionNodeModule,
    AssetsModule,
    CommonModule,
    RouterModule.forChild([{
      path: '',
      component: Component
    }])
  ],
  declarations: [ Component ],
})
export class Module { }
