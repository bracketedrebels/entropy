import { Component as NgComponent, ChangeDetectionStrategy, ViewEncapsulation } from '@angular/core'
import { ProjectService } from '../project'

@NgComponent({
  templateUrl: 'component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  host: {
    class: 'contents'
  }
})
export class Component {
  constructor(
    public readonly project: ProjectService
  ) { }
}
