import { RendererFactory2 } from '@angular/core';

export const createRendererFromFactory = (factory: RendererFactory2) => factory.createRenderer(null, null);
