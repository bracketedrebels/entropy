import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { Component } from './component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { RouterModule } from '@angular/router';
import { NotificationsModule } from './notifications';
import { UpdatesModule } from './updates';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { IconsModule } from 'src/kit/icon';
import { VariableModule } from 'src/kit/variable';
import { FunctionNodeModule } from './ui/nodes/function';
import { AssetsModule } from './assets';
import { ButtonModule } from './ui/button';
import { Guard as ProjectActiveGuard } from './guard';

@NgModule({
  declarations: [ Component ],
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    NotificationsModule,
    HttpClientModule,
    RouterModule.forRoot([{
      path: 'oauth2cb',
      pathMatch: 'prefix',
      loadChildren: () => import('./authorization/+oauth2cb/module').then(m => m.Module)
    },{
      path: 'desk',
      loadChildren: () => import('./+desk/module').then(m => m.Module),
      canActivate: [ ProjectActiveGuard ],
      pathMatch: 'prefix'
    }, {
      path: 'welcome',
      loadChildren: () => import('./+welcome/module').then(m => m.Module),
      pathMatch: 'prefix'
    }, {
      path: '',
      redirectTo: '/welcome',
      pathMatch: 'full'
    }]),
    
    IconsModule,
    VariableModule,
    FunctionNodeModule,
    AssetsModule,
    ButtonModule,
    UpdatesModule
  ],
  bootstrap: [ Component ]
})
export class Module { }
