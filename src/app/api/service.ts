import { Injectable } from "@angular/core"
import { AuthorizationService } from '../authorization'
import { Observable } from 'rxjs'
import { HttpClient } from '@angular/common/http'
import { APICommand } from './types';
import { Key } from '../authorization/consts';

@Injectable({
  providedIn: 'root'
})
export class Service {
  public executeCommand<T>(cmd: APICommand<T>): Observable<T> {
    return cmd({
      gitlab: {
        http: this.client,
        token$: this.authorization.getToken(Key.tokenGitlab)
      }
    });
  }

  constructor(
    private readonly authorization: AuthorizationService,
    private readonly client: HttpClient,
  ) { }
}
