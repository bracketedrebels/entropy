import { environment } from 'src/environments/environment';

export const gql = (literals: TemplateStringsArray, ...placeholders: string[]) => {
  let result = "";

  for (let i = 0; i < placeholders.length; i++) {
      result += literals[i];
      result += placeholders[i];
  }

  result += literals[literals.length - 1];
  return result;
}

export const fullGitlabRESTUri = (suffix: string) => environment.gitlab.api.rest.uri + suffix;
export const fullGitlabGQLUri = (suffix: string) => environment.gitlab.api.gql.uri + suffix;

export const enum BorderErrors { ResponseValidationFailure }