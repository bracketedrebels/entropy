import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

export type APICommand<T> = (v: {
  gitlab: {
    token$: Observable<string>;
    http: HttpClient;
  };
}) => Observable<T>