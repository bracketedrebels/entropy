import { APICommand } from '../types';
import { switchMap } from 'rxjs/operators';
import { fullGitlabRESTUri, BorderErrors } from '../consts';
import { throwError, of } from 'rxjs';

export default () => (
  ({gitlab: { http, token$ }}) => token$
    .pipe(
      switchMap(v => http
        .get(
          fullGitlabRESTUri('/namespaces'),
          { headers:
            { 'Authorization': `Bearer ${v}`
            }
          }
        )
      )
    , switchMap(items => Array.isArray(items) && items.every(v => validate(v))
      ? of(items)
      : throwError(BorderErrors.ResponseValidationFailure))
    )
  ) as APICommand<ResponseItem[]>

export type ResponseItem = {
  id: number;
  name: string;
  path: string;
  kind: ('user' | 'group') & string;
  full_path: string;
}

function validate(data: any): data is ResponseItem {
  return true
    && typeof data.id === 'number'
    && typeof data.name === 'string'
    && typeof data.path === 'string'
    && typeof data.full_path === 'string'
    && (data.kind === 'user' || data.kind === 'group')
}
