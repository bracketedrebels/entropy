import { Injectable } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';

@Injectable({
  providedIn: 'root'
})
export class Service {
  public readonly availible$ = this.updates.available;
  public readonly ready$ = this.updates.activated;

  constructor( private readonly updates: SwUpdate ) { }
}
