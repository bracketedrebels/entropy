import { Component as NgComponent, ViewChild, ChangeDetectionStrategy, TemplateRef, AfterViewInit, OnDestroy } from '@angular/core';
import { NotificationDirective, NotificationsService } from '../notifications';
import { Service } from './service';
import { Subscription } from 'rxjs';
import { filter, tap } from 'rxjs/operators';

@NgComponent({
  templateUrl: 'component.html',
  selector: 'integrate-application-updates[brs]',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class Component implements AfterViewInit, OnDestroy {
  public restart(): void { location.reload() }
  public complete(n: TemplateRef<void>): void { this.notificationsService.complete(n, () => {}) }
  public ngAfterViewInit(): void {
    this.subscription = this.updatesService.ready$
      .pipe(
        filter(() => !!this.notificationUpdateReady))
      .subscribe(() => this.notificationsService
        .enqueue(this.notificationUpdateReady.template))
  }
  public ngOnDestroy(): void {
    this.subscription = this.subscription && void this.subscription.unsubscribe();
  }

  constructor(
    private readonly updatesService: Service,
    private readonly notificationsService: NotificationsService
  ) { }



  @ViewChild('UpdateReady', { static: true }) private readonly notificationUpdateReady: NotificationDirective;
  private subscription: Subscription;
}
