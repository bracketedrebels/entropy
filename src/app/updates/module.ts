import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Component } from './component';
import { NotificationsModule } from '../notifications';

@NgModule({
  imports: [
    CommonModule,
    NotificationsModule
  ],
  declarations: [ Component ],
  exports: [ Component ]
})
export class Module { }
