import { of, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AssetDescriptor, AssetType, AssetsFilterDescriptor } from './types';
import { map, share } from 'rxjs/operators';
import { filterAssets } from './consts';

@Injectable({
  providedIn: 'root'
})
export class Service {
  public filter(f: AssetsFilterDescriptor): Observable<AssetDescriptor[]> {
    return this.assets$
      .pipe(
        map(assets => filterAssets(assets, f)),
        share())
  }
  // @TODO: replace currect temp solution with real data
  public readonly assets$: Observable<AssetDescriptor[]> = of(
    [ { namespace: 'boolean'
      , type: AssetType.function
      , contents:
        { name: 'and'
        , description: 'simple boolean logics conjunction operator'
        , input:
          [ { name: 'a'
            , type: 'boolean'
            }
          , { name: 'b'
            , type: 'boolean'
            }
          ]
        , return:
          { description: 'result of two booleans conjunction'
          , type: 'boolean'
          }
        }
      }
    , { namespace: 'boolean'
      , type: AssetType.function
      , contents:
        { name: 'or'
        , description: 'simple boolean logics disjunction operator'
        , input:
          [ { name: 'a'
            , type: 'boolean'
            }
          , { name: 'b'
            , type: 'boolean'
            }
          ]
        , return:
          { description: 'result of two booleans disjunction'
          , type: 'boolean'
          }
        }
      }
    , { namespace: 'boolean'
      , type: AssetType.function
      , contents:
        { name: 'not'
        , description: 'simple boolean logics negation operator'
        , input:
          [ { name: 'value'
            , type: 'boolean'
            }
          ]
        , return:
          { description: 'negated input value'
          , type: 'boolean'
          }
        }
      }
    ]
  );
}
