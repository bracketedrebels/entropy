import { NgModule } from '@angular/core';
import { Pipe as FilterPipe } from './filter.pipe';

@NgModule({
  declarations: [ FilterPipe ],
  exports: [ FilterPipe ]
})
export class Module { }
