export type AssetDescriptor<T = any> = {
  namespace: string;
  type: AssetType;
  keywords?: string[];
  contents: T;
}

export const enum AssetType {
  function
}

export type AssetsFilterDescriptor = {
  search?: string;
  type?: AssetType;
}