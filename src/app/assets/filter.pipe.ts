import { Pipe as NgPipe, PipeTransform } from '@angular/core';
import { AssetDescriptor, AssetsFilterDescriptor } from './types';
import { filterAssets } from './consts';

@NgPipe(
  { name: 'filter'
  , pure: true
  }
)
export class Pipe implements PipeTransform {
  public transform(v: AssetDescriptor[], f: AssetsFilterDescriptor): AssetDescriptor[] {
    return filterAssets(v, f);
  }
}