import { AssetDescriptor, AssetsFilterDescriptor } from './types';

export const filterAssets = (assets: AssetDescriptor[], filter: AssetsFilterDescriptor) => assets
  .filter(v => {
    const searchFound = !!filter.search
      ? [...v.keywords, v.namespace].includes(filter.search)
      : true;
    const typeFound = !!filter.type
      ? v.type === filter.type
      : true;
    return searchFound && typeFound;
  })