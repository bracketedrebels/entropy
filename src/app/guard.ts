import { Injectable } from '@angular/core'
import { ProjectService } from './project'
import { CanActivate, Router } from '@angular/router'
import { Observable, of } from 'rxjs'
import { tap, take, concatMap } from 'rxjs/operators'

@Injectable({
  providedIn: 'root',
  deps: [ ProjectService, Router ]
})
export class Guard implements CanActivate {
  public canActivate(): Observable<boolean> {
    return this.project.meta$
      .pipe(
        take(1),
        concatMap(v => v
          ? of(true)
          : of(false)
            .pipe(tap(() => this.router.navigate(['/welcome'])))))
  }

  constructor(
    private readonly project: ProjectService,
    private readonly router: Router
  ) { }
}
