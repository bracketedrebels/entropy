import { Component as NgComponent, TemplateRef, ChangeDetectionStrategy, ViewChild, AfterViewInit, OnDestroy, ElementRef } from '@angular/core'
import { AssetsService } from './assets'
import { NotificationsService } from './notifications'
import { ModalsService } from './ui/modals'
import * as trianglify from 'trianglify'
import { ViewportService } from 'src/kit/viewport'
import { Subscription } from 'rxjs';
import { scan, distinctUntilChanged, map, shareReplay } from 'rxjs/operators';
import { AssistanceService } from './ui/contextual';
import { fadeOutDownOnLeaveAnimation, fadeInUpOnEnterAnimation, fadeInOnEnterAnimation, fadeOutOnLeaveAnimation } from 'angular-animations';


@NgComponent({
  selector: 'app-root',
  templateUrl: 'component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    fadeOutDownOnLeaveAnimation({
      duration: 200,
      translate: '5rem'
    }),
    fadeInUpOnEnterAnimation({
      duration: 200,
      translate: '5rem'
    }),
    fadeInOnEnterAnimation(),
    fadeOutOnLeaveAnimation(),
  ]
})
export class Component implements AfterViewInit, OnDestroy{
  public readonly assets$ = this.assetsService.assets$
  public readonly notifications$ = this.notificationsService.notifications$
  public readonly modals$ = this.modalsService.delegations$
  public readonly assistants$ = this.assistanceService.delegations$
  public readonly vpsize$ = this.viewportService.resized$
  public readonly pattern$ = this.viewportService.resized$
    .pipe(
      scan((max, [w, h]) => [w > max[0] ? w : max[0], h > max[1] ? h : max[1]], [0, 0] as [number, number]),
      distinctUntilChanged((a, b) => a && b && a[0] === b[0] && a[1] === b[1]),
      map(([width, height]) => trianglify({width, height, variance: 1, x_colors: ['#fff', '#000'], cell_size: 30, stroke_width: 40})),
      shareReplay(1))

  public readonly notificationTracker = (_, not: TemplateRef<void>) => not
  public readonly templateTracker = (_, template: TemplateRef<void>) => template

  public ngAfterViewInit(): void {
    this.subscription = this.pattern$
      .subscribe(pattern => pattern
        .canvas(this.bgcanvas.nativeElement))
  }

  public ngOnDestroy(): void {
    this.subscription.unsubscribe()
  }

  constructor(
    private readonly assetsService: AssetsService,
    private readonly notificationsService: NotificationsService,
    private readonly modalsService: ModalsService,
    private readonly assistanceService: AssistanceService,
    private readonly viewportService: ViewportService,
  ) { }



  @ViewChild('bgcanvas', { static: false }) private readonly bgcanvas: ElementRef<HTMLCanvasElement>
  private subscription: Subscription
}
