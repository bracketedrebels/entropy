import { HelixPoint } from './types';
import { Vector, mid, yset, y, resize } from '../vector';

export const helixInBox =
  ({helix, box}: { helix: HelixPoint, box: [Vector, Vector] }) =>
  resize({ vectors: box, factor: helixNormalized(helix) })

export const helixNormalized =
  (helix: HelixPoint) => {
    switch( helix ) {
      case 2: return [.5, 0]
      case 3: return [1, 0]
      case 4: return [1, .5]
      case 5: return [1, 1]
      case 6: return [.5, 1]
      case 7: return [0, 1]
      case 8: return [0, .5]
      case 9: return [.5, .5]
      case 1:
      default:
    }
    return [0, 0];
  }