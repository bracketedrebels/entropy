import { Pipe as NgPipe, PipeTransform } from '@angular/core';
import { x, y, Vector, yset, sub } from 'src/kit/vector';

@NgPipe({
  name: 'bspline',
  pure: true
})
export class Pipe implements PipeTransform {
  public transform(points: Vector[], control?: Vector): string {
    if (points.length >= 2) {
      // const delta = sub(points[1], points[0]);
      // const coefficient = Math.abs(y(delta) / x(delta)) * ;
      const cp = control || yset(points[1], y(points[0]));
      return `M ${x(points[0])} ${y(points[0])} Q ${x(cp)} ${y(cp)}, ${x(points[1])} ${y(points[1])}${points.slice(2).map(v => ` T ${x(v)} ${y(v)}`).join('')}`;
    }
    return '';
  }
}