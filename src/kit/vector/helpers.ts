import { Vector } from './types';
import { zipWith } from 'lodash'

export const from = <T>(v: {x: T} | {x: T; y: T} | {x: T; y: T; z: T}) => [v.x, ...('y' in v ? [v.y] : []), ...('z' in v ? [v.z] : [])] as Vector<T>
export const fromClientRect = (rect: ClientRect) => [[rect.left, rect.top], [rect.right, rect.bottom]] as [Vector, Vector];

export const bounds = (vecs: Vector[]) => 
  zipWith(...vecs, (...coords: number[]) => ({ min: Math.min(...coords), max: Math.max(...coords) }))
  .reduce(([tl, br], {min, max}) => [[...tl, min], [...br, max]], [[], []] as [Vector, Vector]) as [Vector, Vector]

export const x = (v: Vector) => v[0]
export const y = (v: Vector) => v[1]
export const z = (v: Vector) => v[2]

export const set = <T>(vec: Vector<T>, index: number, newvalue: T) => vec.map((v, i) => i === index ? newvalue : v)
export const xset = <T>(vec: Vector<T>, newvalue: T) => set(vec, 0, newvalue)
export const yset = <T>(vec: Vector<T>, newvalue: T) => set(vec, 1, newvalue)
export const zset = <T>(vec: Vector<T>, newvalue: T) => set(vec, 2, newvalue)

export const sum = (v1: Vector, ...rest: Vector[]) => zipWith(...[v1, ...rest], (...vec: number[]) => vec.reduce((a, b) => a + b, 0))
export const sub = (v1: Vector, ...rest: Vector[]) => zipWith(...[v1, ...rest], (...vec: number[]) => vec.slice(1).reduce((a, b) => a - b, vec[0]))

export const mul = (v1: Vector, ...rest: (number | Vector)[]) => rest
  .reduce((acc: Vector, other) => Array.isArray(other)
    ? zipWith(acc, other, (a, b) => a * b)
    : acc.map(v => v * other), v1) as Vector

export const resize = ({factor, vectors}: { vectors: Vector[]; factor: number | Vector }) => Array.isArray(factor)
  ? vectors
      .reduce((acc, v) =>
        zipWith(acc, v, factor, (a, b, c) => a + (b - a) * c ), vectors[0])
  : vectors
      .reduce((acc, v) =>
        zipWith(acc, v, (a, b) => a + (b - a) * factor ), vectors[0])

export const mid = (vectors: Vector[]) => resize({ vectors, factor: .5 })


