import { fromEvent, merge, animationFrameScheduler } from 'rxjs'
import { map, observeOn, debounceTime, distinctUntilChanged, shareReplay, startWith, tap } from 'rxjs/operators'
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class Service {
  public readonly resized$ =
    merge(
      fromEvent(window, 'resize', { passive: true }),
      fromEvent(window, 'orientationchange', { passive: true }))
    .pipe(
      debounceTime(200), // empiric value... Welcome to make it more meaningful
      observeOn(animationFrameScheduler),
      startWith(null),
      map(() => [window.innerWidth, window.innerHeight] as [number, number]),
      distinctUntilChanged((a, b) => a && b && a[0] === b[0] && a[1] === b[1]),
      shareReplay(1))
}
