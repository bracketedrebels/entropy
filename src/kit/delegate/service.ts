import { TemplateRef } from '@angular/core'
import { Subject, Observable, asyncScheduler } from 'rxjs'
import { scan, shareReplay, startWith, observeOn, merge, map, tap } from 'rxjs/operators'

export class Service {
  public activate(template: TemplateRef<void>): void { this.subjectActivate.next(template) }
  public deactivate(template: TemplateRef<void>): void { this.subjectDeactivate.next(template) }
  public readonly delegations$: Observable<TemplateRef<void>[]>

  constructor( ) {
    this.delegations$ = this.subjectActivate
      .pipe(
        map(template => ({ template, toggle: true })),
        merge(this.subjectDeactivate
          .pipe(
            map(template => ({template, toggle: false})))),
        scan((acc, entry) => entry.toggle
          ? [...acc.filter(v => v !== entry.template), entry.template]
          : acc.filter(v => v !== entry.template), [] as TemplateRef<void>[]),
        observeOn(asyncScheduler),
        startWith([] as TemplateRef<void>[]),
        shareReplay(1))
  }

  private readonly subjectActivate = new Subject<TemplateRef<void>>()
  private readonly subjectDeactivate = new Subject<TemplateRef<void>>()
}
