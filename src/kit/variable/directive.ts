import { Directive as NgDirective, Input } from '@angular/core';

@NgDirective({
  selector: '[var]',
  exportAs: 'var',
})
export class Directive {
  @Input() public var: any;
}