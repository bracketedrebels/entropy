import { NgModule } from '@angular/core';
import { Directive } from './directive';

@NgModule({
  declarations: [Directive],
  exports: [Directive]
})
export class Module { }
