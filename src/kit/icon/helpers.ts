import { environment } from 'src/environments/environment';

export const makeAssetPath = (name: string) => `${environment.iconsPath}/${name}.svg`;