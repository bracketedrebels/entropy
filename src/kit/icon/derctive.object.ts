import { Directive as NgDirective, Input, HostBinding, OnChanges, SimpleChanges } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { makeAssetPath } from './helpers';

@NgDirective({
  selector: 'object[brs-icon]',
  exportAs: 'brs-icon',
  host: {
    type: 'image/svg+xml'
  }
})
export class Directive implements OnChanges {
  @Input('brs-icon') public name: string;

  constructor( private sanitizer: DomSanitizer ) { }

  public ngOnChanges({ name }: SimpleChanges): void {
    this.srcProperty = this.sanitizer.bypassSecurityTrustResourceUrl(makeAssetPath(name.currentValue));
  }

  @HostBinding('data') private srcProperty: SafeUrl;
}