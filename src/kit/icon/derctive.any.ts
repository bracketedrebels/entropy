import { Directive as NgDirective, Input, OnChanges, SimpleChanges, OnDestroy, ElementRef } from '@angular/core';
import { BehaviorSubject, asyncScheduler } from 'rxjs';
import { switchMap, observeOn } from 'rxjs/operators';
import { Serivce } from './service';

@NgDirective({
  selector: ':not(svg):not(object):not(img)[brs-icon]',
  exportAs: 'brs-icon',
})
export class Directive implements OnChanges, OnDestroy {
  @Input('brs-icon') public name: string;

  constructor(
    private readonly service: Serivce,
    private readonly eref: ElementRef<HTMLElement>
  ) { }

  public ngOnChanges({ name }: SimpleChanges): void {
    if (name.currentValue) this.subjectGet$.next(name.currentValue);
  }

  public ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  private readonly subjectGet$ = new BehaviorSubject<string>('artboard');
  private readonly subscription = this.subjectGet$
    .pipe(
      observeOn(asyncScheduler),
      switchMap(name => this.service
        .get(name)))
    .subscribe(svg => {
      // @TODO support angular universal
      // currently this solution violates angular
      this.eref.nativeElement.innerHTML = svg;
    })
}