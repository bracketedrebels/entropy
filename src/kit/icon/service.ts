import { Injectable, RendererFactory2 } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { makeAssetPath } from './helpers';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class Serivce {
  public get(name: string): Observable<string> {
    return this.cache[name]
      ? of(this.cache[name])
      : this.httpClient
          .get(makeAssetPath(name), { responseType: 'text' })
          .pipe(
            tap(v => this.cache[name] = v));
  }

  public parse(str: string): SVGElement {
    this.placeholder.innerHTML = str;
    const svg = this.placeholder.querySelector('svg') as SVGElement;
    return svg;
  }

  constructor(
    private readonly httpClient: HttpClient,
    private readonly rendererFactory: RendererFactory2,
  ) { }

  private readonly cache: {[i: string]: string} = {};
  private readonly renderer = this.rendererFactory.createRenderer(null, null);
  private readonly placeholder: HTMLDivElement = this.renderer.createElement('div');
}