import { NgModule } from '@angular/core';
import { Directive as ImgDirective } from './derctive.img';
import { Directive as ObjectDirective } from './derctive.object';
import { Directive as SvgDirective } from './derctive.svg';
import { Directive as AnyDirective } from './derctive.any';

@NgModule({
  declarations: [ ImgDirective, ObjectDirective, SvgDirective, AnyDirective ],
  exports: [ ImgDirective, ObjectDirective, SvgDirective, AnyDirective ]
})
export class Module { }