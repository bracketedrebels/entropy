import { Directive as NgDirective, Input, OnChanges, SimpleChanges, OnDestroy, ElementRef } from '@angular/core';
import { BehaviorSubject, asyncScheduler } from 'rxjs';
import { switchMap, observeOn, map } from 'rxjs/operators';
import { Serivce } from './service';

@NgDirective({
  selector: 'svg[brs-icon]',
  exportAs: 'brs-icon'
})
export class Directive implements OnChanges, OnDestroy {
  @Input('brs-icon') public name: string;

  constructor(
    private readonly service: Serivce,
    private readonly eref: ElementRef<SVGElement>
  ) { }

  public ngOnChanges({ name }: SimpleChanges): void {
    if (name.currentValue) this.subjectGet$.next(name.currentValue);
  }

  public ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  private readonly subjectGet$ = new BehaviorSubject<string>('artboard');
  private readonly subscription = this.subjectGet$
    .pipe(
      observeOn(asyncScheduler),
      switchMap(name => this.service
        .get(name)),
      map(src => this.service.parse(src)))
    .subscribe(svg => {
      // @TODO support angular universal
      // currently this solution violates angular
      this.eref.nativeElement.innerHTML = svg.innerHTML;
      const viewbox = svg.getAttribute('viewBox');
      if (viewbox) this.eref.nativeElement.setAttribute('viewBox', viewbox);
      const width = svg.getAttribute('width');
      if (width) this.eref.nativeElement.setAttribute('width', width);
      const height = svg.getAttribute('height');
      if (height) this.eref.nativeElement.setAttribute('height', height);
    })
}