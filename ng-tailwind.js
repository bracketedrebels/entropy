module.exports = {
  // Tailwind Paths
  configJS: 'C:\\development\\bracketedrebels\\entropy\\tailwind.config.js',
  sourceCSS: 'C:\\development\\bracketedrebels\\entropy\\src\\tailwind.scss',
  outputCSS: 'C:\\development\\bracketedrebels\\entropy\\src\\styles.css',
  // Sass
  sass: true,
  // PurgeCSS Settings
  purge: false,
  keyframes: false,
  fontFace: false,
  rejected: false,
  whitelist: [],
  whitelistPatterns: [],
  whitelistPatternsChildren: [],
  extensions: [ '.html' ],
  extractors: [],
  content: []
}
