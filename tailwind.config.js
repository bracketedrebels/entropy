const fs = require('fs');
const path = require('path');
const { entries, reduce } = require('lodash');

const colorLinks
= { "primary": "var(--color-primary)"
  , "accent": "var(--color-accent-8)"
  , "shadow": "var(--color-shadow-15)"
  , "surface":
    { "main": "var(--color-surface-main)"
    , "higher": "var(--color-surface-higher)"
    , "lower": "var(--color-surface-lower)"
    }
  , "info":
    { "error": "var(--color-info-error)"
    , "warn": "var(--color-info-warn)"
    , "ok": "var(--color-info-ok)"
    }   
  }

const colors = stroke = fill
= reduce(
    entries( flattenDict( colorLinks ) )
    , (acc, [name, link]) => ( { ...acc, [name]: link} )
    , { 'transparent': 'transparent' }
  )

const transitionDuration
= { 'none': '0s'
  , 'fast': '.2s'
  , 'medium': '.4s'
  , 'slow': '.6s'
  }

const transitionProperty
= { 'box-shadow': 'box-shadow'
  , 'opacity': 'opacity'
  , 'bg': 'background-color'
  , 'stroke': 'stroke-color'
  , 'all': 'all'
  }

const sizes = borderWidth = textIndent
= { hair: '1px'
  , 0: 0
  , 1: '.5rem'
  , 2: '1rem'
  , 3: '1.5rem'
  , 4: '2rem'
  , 5: '2.5rem'
  , 6: '3rem'
  , 7: '3.5rem'
  , 8: '4rem'
  }

const filter = backdropFilter
= { 'dof-0': `blur(0)`
  , 'dof-1': `blur(1px)`
  , 'dof-2': `blur(2px)`
  , 'dof-3': `blur(3px)`
  , 'dof-4': `blur(4px)`
  , 'dof-5': `blur(5px)`
  , 'desaturate': `grayscale(100%)`
  , 'dof-3-desaturate': `grayscale(100%) blur(3px)`
  , 'dof-5-desaturate': `grayscale(100%) blur(5px)`
  }

const spacing
= { ...sizes
  , ...Object
    .keys(sizes)
    .reduce((acc, key) => ({
      ...acc,
      [key]: sizes[key],
      [`minus-${key}`]: `-${sizes[key]}`
    }), {})
  , half: '50%'
  , full: '100%'
  , halfvw: '50vw'
  , halfvh: '50vh'
  }

const cursor
= { 'grab': 'grab'
  , 'grabbing': 'grabbing'
  , 'pointer': 'pointer'
  , 'default': 'default'
  , 'move': 'move'
  }

const inset
= { ...sizes
  , 'half': '50%'
  }

const fontFamily
= { 'main': "'Roboto', sans"
  , 'symbols': "'Crimson Text', serif"
  }

const linearGradients
= { colors:
    { ...Object
      .keys(colors)
      .reduce((acc1, key1, index) => (
        { ...acc1
        , ...Object
          .keys(colors)
          .slice(index)
          .reduce((acc2, key2) => (
            { ...acc2
            , [`${key1}-${key2}`]: [colors[key1], colors[key2]]
            }
          ), {})
        }
      ), {})
    }
  }

const rotate
= { '90': '90deg'
  , '180': '180deg'
  , '270': '270deg'
  , '0': '0deg'
  }

const borderRadius = height = width
= { ...sizes
  , default: '2px'
  , half: '50%'
  , full: '100%'
  }

const opacity
= { 0: 0
  , 10: .1
  , 20: .2
  , 30: .3
  , 40: .4
  , 50: .5
  , 60: .6
  , 70: .7
  , 80: .8
  , 90: .9
  , 100: 1
  }

const zIndex
= { default: 0
  , low: -1
  , lower: -2
  , lowest: -3
  , high: 1
  , higher: 2
  , highest: 3}

const animations
= { 'zoom-in':
    { from:
      { transform: `scale(0)`
      , opacity: 0
      }
    , to:
      { transform: `scale(1)`
      , opacity: 1
      }
    }
  , 'zoom-out':
    { from:
      { transform: `scale(1)`
      , opacity: 1
      }
    , to:
      { transform: `scale(0)`
      , opacity: 0
      }
    }
  , 'shift-inline-end':
    { from:
      { transform: `translateX(0)`
      }
    , to:
      { transform: `translateX(100%)`
      }
    }
  }

module.exports
= { theme:
    { extend: {}
    , fontFamily
    , colors
    , filter
    , backdropFilter
    , transitionDuration
    , transitionProperty
    , spacing
    , opacity
    , cursor
    , inset
    , borderWidth
    , textIndent
    , stroke
    , fill
    , linearGradients
    , rotate
    , borderRadius
    , height
    , width
    , zIndex
    , animations
    }
  , variants:
    { height: [ 'before', 'after' ]
    , width: [ 'before', 'after' ]
    , transitionDuration: [ 'before', 'after', 'hover:before', 'focus:before', 'hover:after', 'focus:after' ]
    , transitionProperty: [ 'before', 'after', 'hover:before', 'focus:before', 'hover:after', 'focus:after' ]
    , opacity: [ 'before', 'hover', 'focus', 'hover:before', 'focus:before', 'focus-within:before', 'focus-within:after', 'after', 'hover:after', 'focus:after', 'active:before' ]
    , linearBorderGradients: [ 'before', 'after' ]
    , linearGradients: [ 'before', 'after' ]
    , borderStyle: [ 'before', 'after' ]
    , borderWidth: [ 'before', 'after', 'hover:before' ]
    , borderColor: [ 'before', 'after', 'focus', 'hover', 'focus:before', 'focus-within:before', 'focus-within:after' ]
    , scale: ['hover', 'focus', 'active']
    , borderRadius: [ 'before', 'after' ]
    , empty: [ 'before', 'after' ]
    , position: [ 'before', 'after' ]
    , filter: [ 'before', 'after', 'hover:after' ]
    , inset: [ 'before', 'after' ]
    , backgroundColor: [ 'before', 'after', 'hover:before', 'hover:after', 'hover', 'active', 'focus-within' ]
    , padding: [ 'before', 'after' ]
    , margin: [ 'before', 'after' ]
    , alignSelf: [ 'before', 'after' ]
    , textShadow: [ 'before', 'after' ]
    , fontSize: [ 'before', 'after' ]
    , order: [ 'before', 'after' ]
    , cursor: [ 'before', 'after' ]
    , fontFamily: [ 'before', 'after' ]
    , float: [ 'before', 'after' ]
    , zIndex: [ 'active', 'before', 'after' ]
    , flexDirection: [ 'responsive' ]
    , stroke: ['active', 'responsive']
    , pointerEvents: ['after', 'before']
    , textColor: ['focus', 'focus-within', 'hover', 'before', 'after', 'active']
    , mixBlendMode: ['after', 'before']
    , backgroundBlendMode: ['after', 'before']
    , outline: ['hover', 'focus']
    }
  , plugins:
    [ require('tailwindcss-elevation')(['responsive', 'active', 'focus', 'before', 'after'])
    , require('tailwindcss-typography')()
    , require('tailwindcss-filters')()
    , require('tailwindcss-pseudo')()
    , require('tailwindcss-transitions')()
    , require('tailwindcss-border-gradients')()
    , require('tailwindcss-gradients')()
    , require('tailwindcss-blend-mode')()
    , require('tailwindcss-animations')()
    , require('tailwindcss-transforms')({ '3d': true })
    , require('tailwindcss-grid')({
      autoMinWidths: {
        '1': '1rem',
        '2': '2rem',
        '3': '3rem',
        '4': '4rem',
        '5': '5rem',
        '6': '6rem',
        '7': '7rem',
        '8': '8rem',
        '9': '9rem',
        '10': '10rem',
        'auto': 'auto'
      },
      gaps: {
        '1': '.5rem',
        '2': '1rem'
      }
    })
    , ({addVariant, e}) =>
      [ [':hover', ':focus']
      , [':hover', '::before']
      , [':active', '::before']
      , [':hover', '::after']
      , [':focus', ':hover']
      , [':focus', '::before']
      , [':focus', '::after']
      , [':focus-within', '::before']
      , [':focus-within', '::after']
      ]
      .forEach(set => {
        const pseudo = set.join('');
        const variant = pseudo.substr(1).replace('::', ':');
          addVariant(variant, ({ modifySelectors, separator }) =>
            modifySelectors(({ className }) => 
              `.${e(`${variant}${separator}${className}`)}${pseudo}`))})
    ]
  }


function flattenDict(dict, keying = strs => strs.join('-'), path = []) {
  return Object
    .keys(dict)
    .map(key => ({key, value: dict[key]}))
    .reduce((acc, {key, value}) => value !== Object(value) // testing primitiveness
      ? { ...acc, [keying([...path, key])]: value }
      : { ...acc, ...flattenDict(value, keying, [...path, key])}, {});
}
